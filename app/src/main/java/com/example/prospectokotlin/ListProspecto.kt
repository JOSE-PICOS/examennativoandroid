package com.example.prospectokotlin

data class ListProspecto(
    val page: Int,
    val pageSize: Int,
    val prospectos: List<Prospecto>,
    val total: Int
)