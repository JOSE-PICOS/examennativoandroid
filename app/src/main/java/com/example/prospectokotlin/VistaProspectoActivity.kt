package com.example.prospectokotlin

import android.app.DownloadManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.webkit.URLUtil
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.net.CookieManager
import java.net.URI
import java.net.URL

class VistaProspectoActivity : BaseActivity(), ArchivosProspectoRecyclerAdapter.InteractionListener {

    private var prospecto: Prospecto? = null

    private val archivosProspectoRecyclerAdapter: ArchivosProspectoRecyclerAdapter by lazy {
        ArchivosProspectoRecyclerAdapter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_vista_prospecto)

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.vista_prospecto)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        prospecto = intent.extras?.getParcelable(EXTRA_PROSPECTO) as Prospecto?
        val textViewNombre = findViewById<TextView>(R.id.textView_nombre)
        textViewNombre.text = prospecto?.getNombreCompleto()
        val textViewRfc = findViewById<TextView>(R.id.textView_rfc)
        textViewRfc.text = prospecto?.rfc
        val textViewCalle = findViewById<TextView>(R.id.textView_calle)
        textViewCalle.text = prospecto?.calle
        val textViewNumero = findViewById<TextView>(R.id.textView_numero)
        textViewNumero.text = prospecto?.numero
        val textViewColonia = findViewById<TextView>(R.id.textView_colonia)
        textViewColonia.text = prospecto?.colonia
        val textViewCodigoPostal = findViewById<TextView>(R.id.textView_codigo_postal)
        textViewCodigoPostal.text = prospecto?.codigo_postal.toString()
        val textViewComentario = findViewById<TextView>(R.id.textView_comentario)
        textViewComentario.text = prospecto?.comentario_rechazo

        var nombreStatus = ""
        if (prospecto?.status == 1) {
            nombreStatus = "Enviado"
        }

        if (prospecto?.status == 2) {
            nombreStatus = "Autorizado"
        }

        if (prospecto?.status == 3) {
            nombreStatus = "Rechazado"
        }

        var enabledButtons = true
        if (prospecto?.status != 1) {
            enabledButtons = false;
        }

        val textViewStatus = findViewById<TextView>(R.id.textView_status)
        textViewStatus.text = nombreStatus

        val editTextComentario = findViewById<EditText>(R.id.editText_comentario)
        editTextComentario.isVisible = enabledButtons
        textViewComentario.isVisible = !enabledButtons

        val btnRechazar = findViewById<Button>(R.id.btn_rechazar)
        val btnAutorizar = findViewById<Button>(R.id.btn_autorizar)

        btnRechazar.setOnClickListener {
            rechazar()
        }

        btnAutorizar.setOnClickListener {
            autorizar()
        }

        btnRechazar.isVisible = enabledButtons
        btnAutorizar.isVisible = enabledButtons

        cargarArchivos()
    }

    override fun onClick(nombreArchivo: String) {
        downloadFile(nombreArchivo)
    }

    private fun cargarArchivos() {
        val archivos = mutableListOf<String>()
        val archivosRecyclerView = findViewById<RecyclerView>(R.id.archivos_recycler_view)
        archivosRecyclerView.adapter = archivosProspectoRecyclerAdapter
        archivosRecyclerView.layoutManager = LinearLayoutManager(this.baseContext)
        archivosProspectoRecyclerAdapter.submitList(archivos)
        archivosRecyclerView.addItemDecoration(SimpleDividerItemDecoration(baseContext))

        val retrofitGet = RetrofitClient.consumirApi.getFiles(prospecto?._id ?: "")
        retrofitGet.enqueue(object : Callback<List<String>>{
            override fun onResponse(p0: Call<List<String>>, p1: Response<List<String>>) {
                archivosRecyclerView.adapter = archivosProspectoRecyclerAdapter
                archivosProspectoRecyclerAdapter.submitList(p1.body())
                archivosRecyclerView.addItemDecoration(SimpleDividerItemDecoration(baseContext))
            }

            override fun onFailure(p0: Call<List<String>>, p1: Throwable) {
                Toast.makeText(this@VistaProspectoActivity, "Error al consultar Api Rest: ${p1.message}", Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun downloadFile(outputFile: String) {
        try {
            val idProspecto = prospecto?._id ?: ""
            val url = "https://api-prospectos.jorasystems.com/static/$idProspecto/$outputFile"
            val title = URLUtil.guessFileName(url, null, null)
            val request = DownloadManager.Request(Uri.parse(url))
            request.setTitle(title)
            request.setDescription("Descargando archivo: $outputFile")
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
            request.setDestinationInExternalFilesDir(this, Environment.DIRECTORY_DOWNLOADS, title)

            val downloadManager = getSystemService(DOWNLOAD_SERVICE) as DownloadManager
            downloadManager.enqueue(request)
            Toast.makeText( this, "Descarga iniciada", Toast.LENGTH_SHORT).show()
        } catch (e: FileNotFoundException) {
            showSnackbar(this@VistaProspectoActivity, R.id.vista_prospecto, e.message ?: "")
            return  // swallow a 404
        } catch (e: IOException) {
            showSnackbar(this@VistaProspectoActivity, R.id.vista_prospecto, e.message ?: "")
            return  // swallow a 404
        }
    }

    fun rechazar(){
        val editTextComentario = findViewById<EditText>(R.id.editText_comentario)
        val comentario = editTextComentario.text.toString()

        if (comentario.isEmpty()) {
            Toast.makeText( this, "El comentario es requerido", Toast.LENGTH_SHORT).show()
            return
        }

        val retrofitGet = RetrofitClient.consumirApi.rechazar(prospecto?._id ?: "", comentario)
        retrofitGet.enqueue(object : Callback<GenericResponse> {
            override fun onResponse(p0: Call<GenericResponse>, p1: Response<GenericResponse>) {
                if (p1.code() != 200) {
                    showErrorMessage(this@VistaProspectoActivity, R.id.vista_prospecto, p1.errorBody())
                    return
                }

                Toast.makeText(this@VistaProspectoActivity, "Prospecto rechazado exitosamente", Toast.LENGTH_SHORT).show()
                GlobalSession.refrescarListadoProspectos = true
                finish()
            }

            override fun onFailure(p0: Call<GenericResponse>, p1: Throwable) {
                Toast.makeText(this@VistaProspectoActivity, "Error al consultar Api Rest: ${p1.message}", Toast.LENGTH_SHORT).show()
            }
        })
    }

    fun autorizar() {
        val retrofitGet = RetrofitClient.consumirApi.autorizar(prospecto?._id ?: "")
        retrofitGet.enqueue(object : Callback<GenericResponse> {
            override fun onResponse(p0: Call<GenericResponse>, p1: Response<GenericResponse>) {
                if (p1.code() != 200) {
                    showErrorMessage(this@VistaProspectoActivity, R.id.vista_prospecto, p1.errorBody())
                    return
                }

                Toast.makeText(this@VistaProspectoActivity, "Prospecto autorizado exitosamente", Toast.LENGTH_SHORT).show()
                GlobalSession.refrescarListadoProspectos = true
                finish()
            }

            override fun onFailure(p0: Call<GenericResponse>, p1: Throwable) {
                Toast.makeText(
                    this@VistaProspectoActivity,
                    "Error al consultar Api Rest: ${p1.message}",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
    }

    companion object {
        const val EXTRA_PROSPECTO = "Prospecto"
    }
}