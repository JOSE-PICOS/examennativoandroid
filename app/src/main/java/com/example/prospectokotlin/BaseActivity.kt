package com.example.prospectokotlin

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import okhttp3.ResponseBody

open class BaseActivity: AppCompatActivity() {

    fun showErrorMessage(context: Context, resId: Int, responseBody: ResponseBody?) {
        var errores = ""
        responseBody?.let {
            val errorBody = Gson().fromJson(responseBody.string(), ErrorBody::class.java)
            errores = errorBody.getErrores()
        }

        Snackbar.make(context, findViewById(resId), "Error al guardar el prospecto: $errores", Snackbar.LENGTH_LONG)
            .show()
    }

    fun showSnackbar(context: Context, resId: Int, message: String) {
        Snackbar.make(context, findViewById(resId), message, Snackbar.LENGTH_LONG)
                .show()
    }
}