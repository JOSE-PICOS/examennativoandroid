package com.example.prospectokotlin

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration

class SimpleDividerItemDecoration(context: Context?) : ItemDecoration() {
    private val mDivider: Drawable?
    private var leftMargin = 0
    private var isLeftMarginSet = false

    init {
        mDivider = ContextCompat.getDrawable(context!!, R.drawable.line_divider)
    }

    constructor(context: Context?, leftMargin: Int) : this(context) {
        this.leftMargin = leftMargin
        isLeftMarginSet = true
    }

    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        var left = parent.paddingLeft
        if (isLeftMarginSet) {
            left = leftMargin
        }
        val right = parent.width - parent.paddingRight
        val childCount = parent.childCount
        for (i in 0 until childCount) {
            val child = parent.getChildAt(i)
            val params = child.layoutParams as RecyclerView.LayoutParams
            val top = child.bottom + params.bottomMargin
            val bottom = top + mDivider!!.intrinsicHeight
            mDivider.setBounds(left, top, right, bottom)
            mDivider.draw(c)
        }
    }
}