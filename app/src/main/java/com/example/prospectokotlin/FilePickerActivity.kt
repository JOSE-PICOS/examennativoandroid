package com.example.prospectokotlin

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

abstract class FilePickerActivity : BaseActivity() {

    /**
     * Fires an intent to spin up the "file chooser" UI and select an image.
     */
    fun openFilePicker() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            type = "*/*"
            flags = (Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
        }

        startActivityForResult(intent, READ_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?) {
        // The ACTION_OPEN_DOCUMENT intent was sent with the request code
        // READ_REQUEST_CODE. If the request code seen here doesn't match, it's the
        // response to some other intent, and the code below shouldn't run at all.
        if (requestCode == READ_REQUEST_CODE && resultCode == RESULT_OK) {
            // The document selected by the user won't be returned in the intent.
            // Instead, a URI to that document will be contained in the return intent
            // provided to this method as a parameter.
            // Pull that URI using resultData.getData().
            if (resultData != null) {
                val uri = resultData.data
                var path: String? = ""
                uri?.let {
                    path = FileTools.getPath(this@FilePickerActivity, it)
                }
                val data: MutableList<String> = ArrayList(1)
                data.add(path ?: "")
                onPickedFiles(data)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, resultData)
        }
    }

    abstract fun onPickedFiles(pickedFiles: List<String>)

    companion object {
        private const val READ_REQUEST_CODE = 42
    }
}