package com.example.prospectokotlin

import com.google.gson.annotations.SerializedName

class GenericResponse {
    @SerializedName("message")
    var mensaje: String? = null
}