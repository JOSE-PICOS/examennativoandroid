package com.example.prospectokotlin

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.core.net.toUri
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class NuevoProspectoActivity : FilePickerActivity(), FileRecyclerAdapter.InteractionListener {

    private val fileRecyclerAdapter: FileRecyclerAdapter by lazy {
        FileRecyclerAdapter(this)
    }

    private var files: MutableList<File> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_nuevo_prospecto)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.nuevo_prospecto)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        val buttonGuardar = findViewById<Button>(R.id.btn_guardar)
        buttonGuardar.setOnClickListener {
            guardar()
        }

        val buttonSubir = findViewById<Button>(R.id.btn_subir)
        buttonSubir.setOnClickListener {
            subirArchivos()
        }
    }

    override fun onClick(file: File) {
        files.remove(file)
        fileRecyclerAdapter.submitList(files)
        fileRecyclerAdapter.notifyDataSetChanged()
    }

    private fun subirArchivos() {
        openFilePicker()
    }

    private fun guardar() {
        val editTextNombre = findViewById<EditText>(R.id.editText_nombre)
        val editTextApellidoPaterno = findViewById<EditText>(R.id.editText_apellido_paterno)
        val editTextApellidoMaterno = findViewById<EditText>(R.id.editText_apellido_materno)
        val editTextRfc = findViewById<EditText>(R.id.editText_rfc)
        val editTextNumero = findViewById<EditText>(R.id.editText_numero)
        val editTextColonia = findViewById<EditText>(R.id.editText_colonia)
        val editTextCodigoPostal = findViewById<EditText>(R.id.editText_codigo_postal)
        val editTextTelefono = findViewById<EditText>(R.id.editText_telefono)
        val editTextCalle = findViewById<EditText>(R.id.editText_calle)

        val nombre = editTextNombre.text.toString()
        val apellidoPaterno = editTextApellidoPaterno.text.toString()
        val apellidoMaterno = editTextApellidoMaterno.text.toString()
        val rfc = editTextRfc.text.toString()
        val numero = editTextNumero.text.toString()
        val colonia = editTextColonia.text.toString()
        val codigoPostal = editTextCodigoPostal.text.toString()
        val telefono = editTextTelefono.text.toString()
        val calle = editTextCalle.text.toString()

        if (nombre.isEmpty()) {
            Toast.makeText(this, "El nombre es requerido", Toast.LENGTH_SHORT).show()
            return
        }

        if (apellidoPaterno.isEmpty()) {
            Toast.makeText( this, "El apellido Paterno es requerido", Toast.LENGTH_SHORT).show()
        }

        if (rfc.isEmpty()) {
            Toast.makeText( this, "El RFC es requerido", Toast.LENGTH_SHORT).show()
        }

        if (codigoPostal.isEmpty()) {
            Toast.makeText( this, "El Código Postal es requerido", Toast.LENGTH_SHORT).show()
        }

        if (numero.isEmpty()) {
            Toast.makeText( this, "El Número es requerido", Toast.LENGTH_SHORT).show()
        }

        if (telefono.isEmpty()) {
            Toast.makeText( this, "El Teléfono es requerido", Toast.LENGTH_SHORT).show()
        }

        if (calle.isEmpty()) {
            Toast.makeText( this, "La calle es requerido", Toast.LENGTH_SHORT).show()
        }

        if (colonia.isEmpty()) {
            Toast.makeText( this, "La colonia es requerido", Toast.LENGTH_SHORT).show()
        }

        if (files.isEmpty()) {
            Toast.makeText( this, "Se debe agregas uno o más archivos", Toast.LENGTH_SHORT).show()
        }

        val prospecto = Prospecto(
            null,
            apellidoMaterno,
            apellidoPaterno,
            calle,
            codigoPostal.toInt(),
            colonia,
            "",
            nombre,
            numero,
            rfc,
            1,
            telefono
        )

        guardarProspecto(prospecto)
    }

    private fun guardarProspecto(prospecto: Prospecto) {
        val retrofitGet = RetrofitClient.consumirApi.create(prospecto)
        retrofitGet.enqueue(object : Callback<ProspectoResponse> {
            override fun onResponse(p0: Call<ProspectoResponse>, p1: Response<ProspectoResponse>) {
                if (p1.code() != 200) {
                    showErrorMessage(this@NuevoProspectoActivity, R.id.nuevo_prospecto, p1.errorBody())
                    return
                }

                p1.body()?.prospecto?.let {
                    guardarArchivos(it)
                }
            }

            override fun onFailure(p0: Call<ProspectoResponse>, p1: Throwable) {
                Toast.makeText(this@NuevoProspectoActivity, "Error al consultar Api Rest: ${p1.message}", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun guardarArchivos(prospecto: Prospecto) {
        val list = mutableListOf<MultipartBody.Part>()
        files.forEach {
            val requestFile: RequestBody = RequestBody.create(
                MediaType.parse(contentResolver.getType(it.toUri()) ?: ""),
                it
            )
            val body = MultipartBody.Part.createFormData("archivo", it.name, requestFile)
            list.add(body)
        }

        val retrofitGet = RetrofitClient.consumirApi.upload(prospecto._id ?: "", list)
        retrofitGet.enqueue(object : Callback<GenericResponse> {
            override fun onResponse(p0: Call<GenericResponse>, p1: Response<GenericResponse>) {
                if (p1.code() != 200) {
                    showErrorMessage(this@NuevoProspectoActivity, R.id.nuevo_prospecto, p1.errorBody())
                    return
                }

                Toast.makeText(this@NuevoProspectoActivity, "Prospecto agregado exitosamente", Toast.LENGTH_SHORT).show()
                GlobalSession.refrescarListadoProspectos = true
                finish()
            }

            override fun onFailure(p0: Call<GenericResponse>, p1: Throwable) {
                Toast.makeText(this@NuevoProspectoActivity, "Error al consultar Api Rest: ${p1.message}", Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onPickedFiles(pickedFiles: List<String>) {
        files.addAll(pickedFiles.map { File(it) })
        val filesRecyclerView = findViewById<RecyclerView>(R.id.files_recycler_view)
        filesRecyclerView.adapter = fileRecyclerAdapter
        filesRecyclerView.layoutManager = LinearLayoutManager(this.baseContext)
        fileRecyclerAdapter.submitList(files)
        filesRecyclerView.addItemDecoration(SimpleDividerItemDecoration(baseContext))
    }
}