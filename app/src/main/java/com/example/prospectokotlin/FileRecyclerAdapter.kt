package com.example.prospectokotlin

import java.io.File
import android.annotation.SuppressLint
import android.content.Context
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class FileRecyclerAdapter(
    val interactionListener: InteractionListener
): ListAdapter<File, FileRecyclerAdapter.FileViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_file, parent, false)
        return FileViewHolder(view)
    }

    override fun onBindViewHolder(holder: FileViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }

    inner class FileViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bindTo(file: File) {
            val textViewNombre = itemView.findViewById<TextView>(R.id.textView_nombre)
            textViewNombre.text = l

            val btnEliminar = itemView.findViewById<ImageButton>(R.id.btn_eliminar)
            btnEliminar.setOnClickListener {
                interactionListener.onClick(file)
            }
        }
    }

    companion object {

        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<File>() {

            override fun areItemsTheSame(p0: File, p1: File): Boolean {
                return p0.name == p1.name
            }

            override fun areContentsTheSame(p0: File, p1: File): Boolean {
                return p0 == p1
            }
        }
    }

    interface InteractionListener {
        fun onClick(file: File)
    }
}