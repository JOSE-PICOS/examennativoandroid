package com.example.prospectokotlin

import android.annotation.SuppressLint
import android.content.Context
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class ProspectosRecyclerAdapter(
    val interactionListener: InteractionListener
): ListAdapter<Prospecto, ProspectosRecyclerAdapter.ProspectoViewHolder>(DIFF_CALLBACK) {

    var longClickPosition: Int? = null
        set(value) {
            field?.let { notifyItemChanged(it) }
            value?.let { notifyItemChanged(it) }
            field = value
        }

    fun getLongClickItem(): Prospecto? = longClickPosition?.let { getItem(it) }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProspectoViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_prospecto, parent, false)
        return ProspectoViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProspectoViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }

    inner class ProspectoViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        val context: Context get() = itemView.context

        private var isSelected: Boolean = false

        @SuppressLint("SetTextI18n")
        fun bindTo(prospecto: Prospecto) {
            val textViewNombre = itemView.findViewById<TextView>(R.id.textView_nombre)
            textViewNombre.text = prospecto.getNombreCompleto()

            val textViewStatus = itemView.findViewById<TextView>(R.id.textView_status)
            textViewStatus.text = prospecto.status.toString()

            setSelected()

            val layout = itemView.findViewById<ConstraintLayout>(R.id.layout)

            layout.setOnClickListener {
                interactionListener.onClick(prospecto)
            }
        }

        private fun setSelected() {
            val isSelected = adapterPosition == longClickPosition
            val layout = itemView.findViewById<ConstraintLayout>(R.id.layout)

            if (isSelected) {
                if (!this.isSelected) {
                    layout.setBackgroundColor(ContextCompat.getColor(context, R.color.grey_400))
                }
            } else if(this.isSelected) {
                val outValue = TypedValue()
                context.theme.resolveAttribute(android.R.attr.selectableItemBackground, outValue, true)
                layout.setBackgroundResource(outValue.resourceId)
            }

            this.isSelected = isSelected
        }
    }

    companion object {

        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Prospecto>() {

            override fun areItemsTheSame(p0: Prospecto, p1: Prospecto): Boolean {
                return p0._id == p1._id
            }

            override fun areContentsTheSame(p0: Prospecto, p1: Prospecto): Boolean {
                return p0 == p1
            }
        }
    }

    interface InteractionListener {
        fun onClick(prospecto: Prospecto)
    }
}