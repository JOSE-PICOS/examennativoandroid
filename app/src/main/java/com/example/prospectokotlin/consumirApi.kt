package com.example.prospectokotlin

import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import java.io.File

interface consumirApi {
    @GET("prospectos")
    fun findAll(): Call<ListProspecto>

    @POST("prospectos")
    fun create(
        @Body prospecto: Prospecto
    ): Call<ProspectoResponse>

    @Multipart
    @POST("upload/{id}")
    fun upload(
        @Path("id") id: String,
        @Part archivo: MutableList<MultipartBody.Part>
    ): Call<GenericResponse>

    @GET("upload/{id}")
    fun getFiles(
        @Path("id") id: String
    ): Call<List<String>>

    @FormUrlEncoded
    @PATCH("prospectos/rechazar-prospecto/{id}")
    fun rechazar(
        @Path("id") id: String,
        @Field("comentario_rechazo") comentario: String
    ): Call<GenericResponse>

    @PATCH("prospectos/autorizar-prospecto/{id}")
    fun autorizar(
        @Path("id") id: String
    ): Call<GenericResponse>
}