package com.example.prospectokotlin

import android.os.Parcel
import android.os.Parcelable
import java.io.File

data class Prospecto(
    val _id: String? = null,
    val apellido_materno: String,
    val apellido_paterno: String,
    val calle: String,
    val codigo_postal: Int,
    val colonia: String,
    val comentario_rechazo: String,
    val nombre: String,
    val numero: String,
    val rfc: String,
    val status: Int = 1,
    val telefono: String
): Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readInt(),
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readInt(),
        parcel.readString() ?: "",
    ) {
    }

    fun getNombreCompleto(): String {
        return "$nombre $apellido_paterno $apellido_materno"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(_id)
        parcel.writeString(apellido_materno)
        parcel.writeString(apellido_paterno)
        parcel.writeString(calle)
        parcel.writeInt(codigo_postal)
        parcel.writeString(colonia)
        parcel.writeString(comentario_rechazo)
        parcel.writeString(nombre)
        parcel.writeString(numero)
        parcel.writeString(rfc)
        parcel.writeInt(status)
        parcel.writeString(telefono)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Prospecto> {
        override fun createFromParcel(parcel: Parcel): Prospecto {
            return Prospecto(parcel)
        }

        override fun newArray(size: Int): Array<Prospecto?> {
            return arrayOfNulls(size)
        }
    }
}