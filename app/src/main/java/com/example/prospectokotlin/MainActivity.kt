package com.example.prospectokotlin

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity(), ProspectosRecyclerAdapter.InteractionListener {

    private val prospectosRecyclerAdapter: ProspectosRecyclerAdapter by lazy {
        ProspectosRecyclerAdapter(this)
    }

    private var swipeRefreshLayout: SwipeRefreshLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)

        val fabNuevoProspecto = findViewById<FloatingActionButton>(R.id.fab_new_prospecto)
        fabNuevoProspecto.setOnClickListener {
            Toast.makeText(this@MainActivity, "Crear nuevo prospecto", Toast.LENGTH_SHORT).show()
            val intent = Intent(baseContext, NuevoProspectoActivity::class.java)
            startActivity(intent)
        }

        swipeRefreshLayout = findViewById<SwipeRefreshLayout>(R.id.swipe_refresh_layout)
        swipeRefreshLayout?.setOnRefreshListener {
            consultarProspectos()
        }

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        consultarProspectos()
        Permisos.revisarPermisos(this)
    }

    override fun onResume() {
        super.onResume()

        if (GlobalSession.refrescarListadoProspectos) {
            consultarProspectos()
        }
    }

    override fun onClick(prospecto: Prospecto) {
        val intent = Intent(baseContext, VistaProspectoActivity::class.java)
        intent.putExtra(VistaProspectoActivity.EXTRA_PROSPECTO, prospecto)
        startActivity(intent)
    }

    private fun consultarProspectos() {
        val prospectosRecyclerView = findViewById<RecyclerView>(R.id.prospectos_recycler_view)
        prospectosRecyclerView.adapter = prospectosRecyclerAdapter
        prospectosRecyclerView.layoutManager = LinearLayoutManager(this.baseContext)

        val retrofitGet = RetrofitClient.consumirApi.findAll()
        retrofitGet.enqueue(object : Callback<ListProspecto>{
            override fun onResponse(p0: Call<ListProspecto>, p1: Response<ListProspecto>) {
                prospectosRecyclerView.adapter = prospectosRecyclerAdapter
                prospectosRecyclerAdapter.submitList(p1.body()?.prospectos ?: listOf())
                prospectosRecyclerView.addItemDecoration(SimpleDividerItemDecoration(baseContext))
                swipeRefreshLayout?.isRefreshing = false
            }

            override fun onFailure(p0: Call<ListProspecto>, p1: Throwable) {
                swipeRefreshLayout?.isRefreshing = false
                Toast.makeText(this@MainActivity, "Error al consultar Api Rest: ${p1.message}", Toast.LENGTH_SHORT).show()
            }
        })
    }
}