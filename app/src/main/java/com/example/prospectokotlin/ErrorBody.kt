package com.example.prospectokotlin

class ErrorBody {
    var errors = listOf<ErrorGuardado>()

    fun getErrores(): String {
        var errores: String = ""
        errores = errors.joinToString(", ") { it.msg.toString() }

        return errores
    }
}
