package com.example.prospectokotlin

import com.google.gson.annotations.SerializedName

class ProspectoResponse {
    @SerializedName("usuario")
    var prospecto: Prospecto? = null
}
