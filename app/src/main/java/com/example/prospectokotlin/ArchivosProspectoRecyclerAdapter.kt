package com.example.prospectokotlin

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class ArchivosProspectoRecyclerAdapter(
    val interactionListener: InteractionListener
): ListAdapter<String, ArchivosProspectoRecyclerAdapter.ArchivoProspectoViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArchivoProspectoViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_archivo_prospecto, parent, false)
        return ArchivoProspectoViewHolder(view)
    }

    override fun onBindViewHolder(holder: ArchivoProspectoViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }

    inner class ArchivoProspectoViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bindTo(nombreArchivo: String) {
            val textViewNombre = itemView.findViewById<TextView>(R.id.textView_nombre)
            textViewNombre.text = nombreArchivo

            val btnDescargar = itemView.findViewById<ImageButton>(R.id.btn_descargar)
            btnDescargar.setOnClickListener {
                interactionListener.onClick(nombreArchivo)
            }
        }
    }

    companion object {

        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<String>() {

            override fun areItemsTheSame(p0: String, p1: String): Boolean {
                return p0 == p1
            }

            override fun areContentsTheSame(p0: String, p1: String): Boolean {
                return p0.equals(p1)
            }
        }
    }

    interface InteractionListener {
        fun onClick(nombreArchivo: String)
    }
}